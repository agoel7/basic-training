/* Write a program to find the frequency of all elements in the array
String will be in lowercase, do this using array */


function calculateFrequency(string) {
    // Write your code here
    // Creating the array in which we will calculate the frequencies
    let arr = [];

    // traversing the string and increasing the frequency by 1 on every occurence
    for (let i = 0; i < string.length; i++) {
      if (typeof arr[string.charCodeAt(i)] == "undefined") {
        arr[string.charCodeAt(i)] = 0;
      }
      arr[string.charCodeAt(i)] += 1;
    }
    // Creating object to store the final result
    let obj = {};
    for (let i = 0; i < string.length; i++) {
        // We need frequency of only the elements (a-z)
      if (
        string[i] != " " &&
        string.charCodeAt(i) >= 97 &&
        string.charCodeAt(i) <= 122
      ) {
        obj[string[i]] = arr[string.charCodeAt(i)];
      }
    }
    return obj;
  }


  // Method-2

  /*

  function frequency(str) {
  const obj = {};
  for (let i in str) {
    if (typeof obj[str[i]] == "undefined") {
      obj[str[i]] = 1;
    } else {
      obj[str[i]] += 1;
    }
  }
  return obj;
}

  */