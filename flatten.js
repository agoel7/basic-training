// Write a program to flatten the object

const flatten = (ob) => {
    // Creating the object result to store the final result in it
    let result = {};
  
    /* Iterating the object values and if the the value is 
    object then calling the function recursively */ 
    for (const i in ob) {

        // Checking for the type if object calling recursively
      if (typeof ob[i] === "object" ) {
        const temp = flatten(ob[i]);
        // Iterating the result and adding the keys together to a single value
        for (const j in temp) {
          result[i + "." + j] = temp[j];
        }
      }
      else {
        result[i] = ob[i];
      }
    }
    // returning the object.
    return result;
  };