// Write a program to find the second largest element in the array without using sorting


function secondLargest(arr) {
  // Write your code here

//   Taking 2 variables largest and secondLargest
// Assiging min values to both of them in order to find the largest
  let largest=Number.MIN_SAFE_INTEGER
  let secondLargest=Number.MIN_SAFE_INTEGER

/* Looping through the array and checking whether the array 
element is greater then our larger and second larger elements*/ 
  for(let i=0;i<arr.length;i++){
    if(arr[i]>=largest){
      secondLargest=largest
      largest=arr[i]
    }else if(arr[i]<largest && arr[i]>=secondLargest){
      secondLargest=arr[i]
    }
  }
//   returning the secondLargest element
  return secondLargest
}




// Method-2
/*

function secondLargest(arr) {
  let largest = Number.MIN_SAFE_INTEGER;
  let second = Number.MIN_SAFE_INTEGER;
  for (let i in arr) {
    if (arr[i] > largest) {
      largest = arr[i];
    }
  }
  for (let j in arr) {
    if (arr[j] > second && arr[j] < largest) {
      second = arr[j];
    }
  }
  return second;
}

*/